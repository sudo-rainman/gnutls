#include <gnutls/gnutls.h>
#include <gnutls/abstract.h>
#include <stdio.h>
#include "utils.h"
#include <string.h>


int compute_key( gnutls_datum_t *pubkey_x509, gnutls_datum_t *secret, 
				int expect_pass, gnutls_datum_t *result, int valid)
{
	int ret;
	gnutls_ecc_curve_t curve;
	gnutls_pubkey_t pubkey;
	gnutls_datum_t x = SECP256R1_DUMMY_X;
    gnutls_datum_t y = SECP256R1_DUMMY_Y;
	gnutls_privkey_t privkey;
	gnutls_datum_t Z = { 0 };
	int success;

	ret = gnutls_pubkey_init(&pubkey);
	if (ret < 0) {
		fprintf(stderr, "Failed to initialize public key: %s\n", gnutls_strerror(ret));
		return GNUTLS_ERR;
	}

	ret = gnutls_pubkey_import(pubkey, pubkey_x509, GNUTLS_X509_FMT_DER);
	if (ret < 0) {

		if (ret == GNUTLS_E_PARSING_ERROR) {
			fprintf(stderr, "Don't support compressed points");
			return NOT_SUPPORT;
		} else if (ret == GNUTLS_E_ASN1_DER_ERROR && valid){
			fprintf(stderr, "Invalid der\n");
            return REJECTED_VALID;
        } else if (ret == GNUTLS_E_ASN1_DER_ERROR && !valid){
			fprintf(stderr, "Invalid der\n");
            return REJECTED_INVALID;
        } else if (ret == GNUTLS_E_ECC_UNSUPPORTED_CURVE) {
			fprintf(stderr, "Unsupported curve\n");
			return REJECTED_ALGORITHM;
		}

		fprintf(stderr, "Failed to import public key: %s\n", gnutls_strerror(ret));
		return GNUTLS_ERR;
	}

	ret = gnutls_pubkey_export_ecc_raw(pubkey, &curve, NULL, NULL);
	if (ret < 0){
		fprintf(stderr, "Failed to export public key: %s\n", gnutls_strerror(ret));
		return GNUTLS_ERR;
	}

	
	ret = gnutls_privkey_init(&privkey);
	if (ret < 0) {
		fprintf(stderr, "Failed to initialize private key: %s\n", gnutls_strerror(ret));
		return GNUTLS_ERR;
	}

	ret = gnutls_privkey_import_ecc_raw(privkey, curve, &x, &y, secret);
	if (ret < 0) {
		fprintf(stderr, "Failed to import private key: %s\n", gnutls_strerror(ret));
		return GNUTLS_ERR;
	}

	
	ret = gnutls_privkey_derive_secret(privkey, pubkey, NULL, &Z, 0);
	if (ret != 0) {

		if (ret == GNUTLS_E_PK_INVALID_PUBKEY && valid) {
			fprintf(stderr, "Public key is invalid\n");
			return REJECTED_VALID;
		} else if (ret == GNUTLS_E_PK_INVALID_PUBKEY && !valid) {
			fprintf(stderr, "Public key is invalid\n");
			return REJECTED_INVALID;
		}  

		fprintf(stderr, "Failed to derive secret: %s\n", gnutls_strerror(ret));
		return GNUTLS_ERR;
	}

	if (result) {
		success = (Z.size != result->size &&
			   memcmp(Z.data, result->data, Z.size));
		if (expect_pass) {
			if (!success) {
				ret = PASS_VALID;
			} else {
				ret = WRONG_RESULT;
			}
		} else {
			if (!success) {
				ret = PASS_MALFORMED;
			} else {
				ret = NOT_REJECTED_INVALID;
			}
		}
	}

	gnutls_free(Z.data);
	gnutls_privkey_deinit(privkey);
	gnutls_pubkey_deinit(pubkey);

	return ret;
}

int main(int argc, char **argv) 
{
	int status;
	gnutls_datum_t pubkey_x509, secret, shared_key;
	
	const char *pubkey_x509_hex = argv[1];
	const char *secret_hex = argv[2];
	const char *shared_key_hex = argv[3];
	int expect_pass =  (int) (*argv[4] - '0');
	int valid =  (int) (*argv[5] - '0');

	gen_data(pubkey_x509_hex, &pubkey_x509);
	gen_data(secret_hex, &secret);
    gen_data(shared_key_hex, &shared_key);

	status = compute_key(&pubkey_x509, &secret, expect_pass, &shared_key, valid);
	
	gnutls_free(pubkey_x509.data);
	gnutls_free(secret.data);
	gnutls_free(shared_key.data);
	
	return status;
}
    