import subprocess
import json
import argparse

TEST_STATUS = {
    255: "GNUTLS_ERR",
    0: "PASS_VALID",
    1: "PASS_MALFORMED",
    2: "WRONG_RESULT",
    3: "NOT_REJECTED_INVALID",
    4: "REJECTED_ALGORITHM",
    5: "REJECTED_VALID",
    6: "REJECTED_INVALID",
    7: "WRONG_EXCEPTION",
    8: "NOT_SUPPORT"
}

parser = argparse.ArgumentParser(description="Read test JSON file")
parser.add_argument('JsonFile', metavar='JsonFile', type=str, help='the path to the JSON file')
parser.add_argument('ExeFile', metavar='ExeFile', type=str, help='the path to the test EXE file')

args = parser.parse_args()

with open(args.JsonFile, 'r') as f:
    data = json.load(f)

testGroup = data["testGroups"][0]
testType = testGroup["type"]

print("========================================================================")
print("Test type: " + testType)

if "Ecdh" in testType:

    testCurve = testGroup["curve"]
    print("Test curve: " + testCurve)
    testEncoding = testGroup["encoding"]
    print("Test encoding: " + testEncoding)
    print("========================================================================")

    for test in testGroup["tests"]:

        testID = test["tcId"]
        print("Test ID: " + str(testID))
        testResult = test["comment"]
        print("Test bug type: " + testResult) 
    
        pubkey_x509_hex = test["public"]
        secret_hex = test["private"]
        shared_key_hex = test["shared"]
        expect_pass = "1" if test["result"] in ["valid", "acceptable"] else "0"
        valid = "1" if test["result"] in ["valid"] else "0"

        result = subprocess.run([args.ExeFile,test["public"],test["private"],test["shared"], expect_pass,valid], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        
        return_code = result.returncode
        # stdout_output = result.stdout.strip(b"\n").decode()
        stderr_output = result.stderr.strip(b"\n").decode()

        if int(return_code) == 255:
            print(stderr_output)
        else:
            print(TEST_STATUS[return_code])

        print("========================================================================")
