#include <string.h>
#include <gnutls/gnutls.h>

void gen_data(const char *hex_string, gnutls_datum_t *out)
{
    gnutls_datum_t in;

    in.data = hex_string;
    in.size = strlen(hex_string);
    
    gnutls_hex_decode2(&in, out);
}