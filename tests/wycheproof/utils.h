#define SECP256R1_DUMMY_X {"\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01",32}
#define SECP256R1_DUMMY_Y {"\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01",32}
              
typedef enum {
    GNUTLS_ERR = -1,
    PASS_VALID = 0,
    PASS_MALFORMED = 1,
    WRONG_RESULT = 2,
    NOT_REJECTED_INVALID = 3,
    REJECTED_ALGORITHM = 4,
    REJECTED_VALID = 5,
    REJECTED_INVALID = 6,
    WRONG_EXCEPTION = 7,
    NOT_SUPPORT = 8
} test_status;

void gen_data(const char *hex_string, gnutls_datum_t *out);
